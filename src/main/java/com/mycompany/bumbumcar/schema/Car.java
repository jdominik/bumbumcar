/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bumbumcar.schema;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Msi
 */
@Entity
@Table(name = "cars")
public class Car implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private String vin;
    @Column
    private Integer vintage;
    @Column
    private String brand;
    @Column
    private String model;
    @Column
    private String no_oc;
    @Column
    private String no_reg;
    @Column
    private String fuel;
    @Column
    private Integer power;
    
   /* public enum Fuel {
        diesel,
        petrol,
        lpg;
        electric;
            
    } */
    
    @Column
    private Long milage;
    @Column
    private String engine;
    @Column
    private String gearbox;
    @Column
    private String description;
    @Column
    private Integer tests;

    public void setVin(String vin) {
        this.vin = vin;
    }

    public void setVintage(Integer vintage) {
        this.vintage = vintage;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setNo_oc(String no_oc) {
        this.no_oc = no_oc;
    }

    public void setNo_reg(String no_reg) {
        this.no_reg = no_reg;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public void setMilage(Long milage) {
        this.milage = milage;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public void setPower(Integer power) {
        this.power = power;
    }

    public void setGearbox(String gearbox) {
        this.gearbox = gearbox;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setTests(Integer tests) {
        this.tests = tests;
    }

    public String getVin() {
        return vin;
    }

    public Integer getVintage() {
        return vintage;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public String getNo_oc() {
        return no_oc;
    }

    public String getNo_reg() {
        return no_reg;
    }

    public String getFuel() {
        return fuel;
    }

    public Long getMilage() {
        return milage;
    }

    public String getEngine() {
        return engine;
    }

    public Integer getPower() {
        return power;
    }

    public String getGearbox() {
        return gearbox;
    }

    public String getDescription() {
        return description;
    }

    public Integer getTests() {
        return tests;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    
}
