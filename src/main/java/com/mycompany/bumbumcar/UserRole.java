/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bumbumcar;

/**
 *
 * @author RENT
 */

public enum UserRole {

        ADMIN("admin"),
        MOD("moderator"),
        CLIENT("client");
        
        private final String text; 
    
    private UserRole(final String text) {
        this.text = text;
    }
    
    @Override
    public String toString() {
        return text;
    }
    

    
}
