package com.mycompany.bumbumcar;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.mycompany.bumbumcar.schema.Car;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

/**
 *
 * @author RENT
 */
public class IndexServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();

        if (request.getParameter("car_sale_id") != null) {

            if (request.getParameter("remove") != null) {

                Transaction removeTransaction = session.beginTransaction();
                try {
                    Query query = session.createQuery("delete Car as c where c.id = :code");
                    query.setParameter("code", Long.parseLong(request.getParameter("car_sale_id")));
                    int result = query.executeUpdate();
                    removeTransaction.commit();
                } catch (Exception e) {
                    removeTransaction.rollback();
                }
                request.setAttribute("p", request.getParameter("p"));
                request.getRequestDispatcher("index.jsp").forward(request, response);
                return;
            }

            Query query = session.createQuery("from Car as c where c.id = :code");
            query.setParameter("code", Long.parseLong(request.getParameter("car_sale_id")));

            Car car = (Car) query.uniqueResult();
            //List list = query.list();
            //String carId = UUID.randomUUID().toString();
            //request.setAttribute("carid", carId);

            //request.getSession().setAttribute(carId, car);
            request.setAttribute("car", car);
            request.setAttribute("p", request.getParameter("p"));
            request.getRequestDispatcher("index.jsp").forward(request, response);
            //request.setAttribute("car_sell_id", request.getParameter("car_sell_id"));
        } else {
            List list;
            if (request.getParameter("filter") != null) {
                list = session.createQuery("from Car as c where c.brand like :sbrand and c.model like :smodel")
                .setParameter("sbrand", request.getParameter("b") + "%")
                .setParameter("smodel", request.getParameter("m") + "%")
                .setFirstResult(((request.getParameter("start")) != null) ? Integer.parseInt(request.getParameter("start")) : 0)
                .setMaxResults(2)
            .list();

               /* list = session.createQuery(query)
                        .setParameter(0, request.getParameter("b"))
                        .setParameter(1, request.getParameter("m"))
                        .list(); */

            } else {

                Query query = session.createQuery("from Car");
                query.setFirstResult(((request.getParameter("start")) != null) ? Integer.parseInt(request.getParameter("start")) : 0);
                query.setMaxResults(2);

                list = query.list();
            }
            Collections.reverse(list);
            request.setAttribute("cars", list);

            String filepath = request.getServletContext().getRealPath("/img/cars");

            //List<Image> imagelist = null;
            File[] files = new File(filepath).listFiles();
            ArrayList<Image> images = new ArrayList<Image>();
            for (File file : files) {
                if (file.getName().startsWith("id")) {
                    Image image = new Image();
                    image.setTitle(file.getName());
                    String[] parts = file.getName().split("_");
                    String[] id = parts[0].split("id");
                    image.setId(id[1]);

                    images.add(image);
                }
            }

            /*for (String filename : files) {
            
                if (filename.startsWith("id")) {
                    Image image = new Image();
                    String[] parts = filename.split("_");
                    image.setId(parts[0]);
                    image.setTitle(filename);
                    imagelist.add(image);
                }
                
            }*/
            request.setAttribute("images", images);

            request.setAttribute("p", request.getParameter("p"));
            request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        /*response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
             /*TODO output your page here. You may use following sample code. */
 /*  <% @include file="menu.jsp" %>
        out.println("<div class='container>');
        @include file="header.jsp";
        out.println("<div class='row'>");
        <jsp:include page="<%= request.getParameter("p") + ".jsp"%>" flush="true" />
        
        @include file="rightbar.jsp";
        out.println("</div><!-- /.row -->");
        out.println("</div><!-- /.container -->");
        <%@include file="footer.jsp" %> */
 /*    out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet IndexServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet IndexServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
