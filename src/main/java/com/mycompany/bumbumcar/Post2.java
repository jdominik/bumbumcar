/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bumbumcar;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 *
 * @author RENT
 */

@Entity
@Table(name = "posts", uniqueConstraints = {
    @UniqueConstraint(columnNames = "id")
    })
public class Post2 implements Serializable {

        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "id", unique = true, nullable = false)
        private Integer id;

        @Column(name = "title", unique = true, nullable = false, length = 45)
        private String email;

        @Column(name = "content", unique = false, nullable = false, length = 8000)
        private String firstName;

        @Column(name = "date", unique = false, nullable = false, length = 100)
        private String lastName;
        
        //@OneToOne(mappedBy = "id" cascade = CascadeType.ALL)     // mapped by to jest wedle tego co jest w tabeli, z którą się spinamy
        //@Column(name = "userid", unique = false, nullable = false)
        //private Integer lastName;

        //Getters and setters
    
}
