/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bumbumcar.schema;

import com.mycompany.bumbumcar.schema.Car;
import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author Msi
 */
@Entity
@Table(name = "sales")
public class Sale implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column
    private BigDecimal price;


    
    @ManyToOne
    @JoinColumn(name = "carid")
    private Car car;
    
    @ManyToOne
    @JoinColumn(name = "clientid")
    private Client client;
    
    @ManyToOne
    @JoinColumn(name = "contractid")
    private Contract contract;
    
    
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setContract(Contract contract) {
        this.contract = contract;
    }

    public Car getCar() {
        return car;
    }

    public Client getClient() {
        return client;
    }

    public Contract getContract() {
        return contract;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

   
    
}
