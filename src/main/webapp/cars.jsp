<%-- 
    Document   : posts
    Created on : 2017-07-20, 20:50:50
    Author     : RENT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8" language="java"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>BumBum Car Komis</title>
    </head>
    <body>
        <div class="col-sm-8 blog-main">
            
            

            <div class="blog-post">
                <h2 class="blog-post-title">Krótko o nas</h2>
                <p class="blog-post-meta">Sierpień 11, 2017 by <a href="#">BumBum Car</a></p>

                <p>Cum sociis natoque penatibus et magnis <a href="#">dis parturient montes</a>, nascetur ridiculus mus. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Sed posuere consectetur est at lobortis. Cras mattis consectetur purus sit amet fermentum.</p>
                <blockquote>
                    <p>Curabitur blandit tempus porttitor. <strong>Nullam quis risus eget urna mollis</strong> ornare vel eu leo. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                </blockquote>
                <p>Etiam porta <em>sem malesuada magna</em> mollis euismod. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia bibendum nulla sed consectetur.</p>
            </div><!-- /.blog-post -->
            <div class="blog-post">
            <nav>
                <form action="index.do?filter" method="get">
                    <ul class="pager">
                        <li><span class="blog-post-meta">Marka: <input name="b" type="text">
                            Model: <input name="m" type="text"><input name="filter" type="submit" value="Filtruj"></span></li>
                    </ul>
                </form>
            </nav>
            </div><!-- /.blog-post -->
            <c:forEach items="${cars}" var="car">
            <div class="blog-post">
                <h2 class="blog-post-title"><c:out value="${car.brand} ${car.model} ${car.engine}"/></h2>
                
                
                <p>Skrzynia biegów: <c:out value="${car.gearbox}"/></p>
                <p>Moc: <c:out value="${car.power}"/></p>
                <p>Rocznik: <c:out value="${car.vintage}"/></p>
                <p>Paliwo: <c:out value="${car.fuel}"/></p>
                <p>Przebieg: <c:out value="${car.milage}"/></p>
                <p><c:out value="${car.description}"/></p>
                <div><c:forEach items="${images}" var="image">
                        <c:if test="${car.id == Integer.parseInt(image.id)}">
                        <img src="img/cars/${image.title}">
                        <br/>
                        </c:if>
                    </c:forEach></div>
                <form action="index.do?p=sale" method="post">
                    
                    <input name="car_sale_id" value="${car.id}" type="hidden">
                    <input type="submit" name="Create" value="Sprzedaj">
                </form>
                <form action="index.do?p=cars" method="post">
                    
                    <input name="car_sale_id" value="${car.id}" type="hidden">
                    
                    
                    <input type="submit" name="remove" value="Usuń" onclick="return confirm('Na pewno usunąć?')">
                </form>
            </div><!-- /.blog-post -->
            </c:forEach>
       
            <nav>
                <ul class="pager">
                    <li><a class="blog-nav-item active" href="index.do?start=${param.start-2}${(param.b != null) ? '&b='.concat(param.b) : ''}${(param.m != null) ? '&m='.concat(param.m) : ''}${(param.filter != null) ? '&filter' : ''}">Previous</a></li>
                    <li><a class="blog-nav-item active" href="index.do?start=${param.start+2}${(param.m != null) ? '&b='.concat(param.b) : ''}${(param.m != null) ? '&m='.concat(param.m) : ''}${(param.filter != null) ? '&filter' : ''}">Next</a></li>
                </ul>
            </nav>

                
            
        </div><!-- /.blog-main -->
    </body>
</html>
