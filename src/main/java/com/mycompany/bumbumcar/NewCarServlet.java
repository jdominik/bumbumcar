/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bumbumcar;

import com.mycompany.bumbumcar.schema.Car;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.Optional;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import org.apache.commons.fileupload.FileItem;

import org.apache.commons.io.output.*;
import org.apache.commons.fileupload.disk.*;
import org.apache.commons.fileupload.servlet.*;
import org.apache.commons.io.output.*;

/**
 *
 * @author RENT
 */
//@WebServlet("/upload")
@MultipartConfig
public class NewCarServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html;charset=UTF-8");

        request.getRequestDispatcher("/index.do").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        SessionFactory instance = ConfigHibernate.getInstance();
        /*Session openSession = instance.openSession();
        Transaction beginTransaction = openSession.beginTransaction();
        Post post = new Post();
        User user = new User();
        Query createQuery = openSession.createQuery("FROM User WHERE name='" + request.getParameter("username") + "' AND pass= '" + request.getParameter("pass") + "'");

        User result = (User) createQuery.uniqueResult();
        beginTransaction.commit();
        //openSession.close();

        if (result != null) {*/
        Session openSessionSave = instance.openSession();
        Transaction beginTransaction2 = openSessionSave.beginTransaction();
        //beginTransaction2.rollback();
        try {
        Car car = new Car();
        car.setBrand(request.getParameter("brand"));
        car.setModel(request.getParameter("model"));
        car.setEngine(request.getParameter("engine"));
        car.setGearbox(request.getParameter("gearbox"));
        car.setPower(Integer.parseInt(request.getParameter("power")));
        car.setVintage(Integer.parseInt(request.getParameter("vintage")));
        car.setMilage(Long.parseLong(request.getParameter("milage")));
        car.setNo_oc(request.getParameter("no_oc"));
        car.setNo_reg(request.getParameter("no_reg"));
        car.setTests(Integer.parseInt(request.getParameter("tests")));
        car.setVin(request.getParameter("vin"));
        openSessionSave.save(car);
        beginTransaction2.commit();
        } catch(Exception e) {
            beginTransaction2.rollback();
        }
        

        //adding files from form section
        File file;
        int maxFileSize = 5000 * 1024;
        int maxMemSize = 5000 * 1024;
        ServletContext context = request.getServletContext();
        String filePath = context.getInitParameter("file-upload");

        // Verify the content type
        String contentType = request.getContentType();

        if ((contentType.indexOf("multipart/form-data") >= 0)) {
            DiskFileItemFactory factory = new DiskFileItemFactory();
            // maximum size that will be stored in memory
            factory.setSizeThreshold(maxMemSize);

            // Location to save data that is larger than maxMemSize.
            factory.setRepository(new File("c:\\temp"));

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);

            // maximum file size to be uploaded.
            upload.setSizeMax(maxFileSize);
            try {
                // Parse the request to get file items.
                List fileItems = upload.parseRequest(request);

                // Process the uploaded file items
                Iterator i = fileItems.iterator();
                while (i.hasNext()) {
                    FileItem fi = (FileItem) i.next();
                    if (!fi.isFormField()) {
                        // Get the uploaded file parameters
                        String fieldName = fi.getFieldName();
                        String fileName = fi.getName();
                        boolean isInMemory = fi.isInMemory();
                        long sizeInBytes = fi.getSize();

                        // Write the file
                        if (fileName.lastIndexOf("\\") >= 0) {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\")));
                        } else {
                            file = new File(filePath
                                    + fileName.substring(fileName.lastIndexOf("\\") + 1));
                        }
                        fi.write(file);
                    }
                }
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }
            //beginTransaction2.commit();
            //openSession.close();
            processRequest(request, response);

            /*} else {
            try (PrintWriter out = response.getWriter()) {
                /* TODO output your page here. You may use following sample code. 
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet PostServlet</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Wrong identity data!</h1>");
                out.println("</body>");
                out.println("</html>");
            //request.getRequestDispatcher("/index.do").forward(request, response);
            //processRequest(request, response);
        }
        
    }*/
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
            () {
        return "Short description";
        }// </editor-fold>

    }
