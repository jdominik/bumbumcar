/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.bumbumcar;

import com.mycompany.bumbumcar.schema.User;
import java.util.List;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author Msi
 */
public class UserSession {
    private Boolean logged = Boolean.FALSE;
    private Boolean isAdmin = Boolean.FALSE;
    private String username;
    
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
    
        public List<User> getUsers(User user) {
        //Query query = em.createQuery("FROM User ");
        //return query.getResultList();
        List list;
        String hql = "FROM User u where u.username = :username and u.password = :password";
        list = session.createQuery(hql)
                .setParameter("username", user.getUsername())
                .setParameter("password", user.getPassword())
                .list();
        return list;
    }

    
    public void registerUser(User user) {
        this.setIsAdmin(Boolean.FALSE);
    }

    
    public String checkUser(User user) {

        List<User> userList = this.getUsers(user);
        if (userList.isEmpty()) {
            this.setLogged(Boolean.FALSE);
            return "/login.xhtml";
        }

        user.setUsername(userList.get(0).getUsername());
        this.setIsAdmin((userList.get(0).getRole().equals("admin")));
        this.setLogged(Boolean.TRUE);

        return "/index.xhtml";

    }
    public Boolean getLogged() {
        return logged;
    }

    public void setLogged(Boolean logged) {
        this.logged = logged;
    }



    public void setIsAdmin(Boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    
    public UserSession() {
    }

}
